﻿using System.Collections.Generic;
using Twinkle_milestone2codingtest_09sept3pm.Entities;

namespace Twinkle_milestone2codingtest_09sept3pm.Persistance
{
    public interface IGroceryServices
    {
        IEnumerable<Product> ViewAllProducts();
        IEnumerable<MenuBar> ViewListOfCategories();
        public ProductOrder AddOrder(int userid, int productid);
        Product GetProductDetailsById(int productid);
        IEnumerable<ProductOrder> ShowOrderInfoQuery(int userid);
    }
}
