﻿using System.Collections.Generic;
using System.Linq;
using Twinkle_milestone2codingtest_09sept3pm.Entities;
using Twinkle_milestone2codingtest_09sept3pm.Persistance;

namespace Twinkle_milestone2codingtest_09sept3pm.Repository
{

    public class GroceryServices : IGroceryServices
    {

        private readonly GroceriesDbContext _context;

        public GroceryServices(GroceriesDbContext context)
        {
            _context = context;
        }

        public ProductOrder AddOrder(int userid, int productid)
        {
            var orderid = _context.ProductOrder.Max(x => x.OrderId) + 1;
            var product = _context.Product.SingleOrDefault(x => x.ProductId == productid);
            var user = _context.ApplicationUser.SingleOrDefault(x => x.UserId == userid);
            var productorder = new ProductOrder()
            {
                UserId = userid,
                ProductId = productid,
                OrderId = orderid,
                Product = product,
                User = user
            };
            _context.ProductOrder.Add(productorder);
            _context.SaveChanges();
            return productorder;

        }

        public Product GetProductDetailsById(int productid)
        {
            return _context.Product.SingleOrDefault(x => x.ProductId == productid);
        }

        public IEnumerable<ProductOrder> ShowOrderInfoQuery(int userid)
        {
            _context.ProductOrder.SingleOrDefault(x => x.OrderId == userid);
            return _context.ProductOrder.ToList();
        }

        public IEnumerable<Product> ViewAllProducts()
        {
            return _context.Product.ToList();
        }

        public IEnumerable<MenuBar> ViewListOfCategories()
        {
            return _context.MenuBar.ToList();
        }
    }
}
