﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Twinkle_milestone2codingtest_09sept3pm.Entities
{
    public partial class Product
    {
        public Product()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }
    
        public int ProductId { get; set; }

      
        public string ProductName { get; set; }
     
        public string Description { get; set; }
      
        public decimal? Amount { get; set; }
     
        public int? Stock { get; set; }
      
        public int? CatId { get; set; }
       
        public string Photo { get; set; }
        

        public virtual Categoies Cat { get; set; }
        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
