﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Twinkle_milestone2codingtest_09sept3pm.Entities
{
    public partial class MenuBar
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Url { get; set; }
        public bool? OpenInNewWindow { get; set; }
    }
}
