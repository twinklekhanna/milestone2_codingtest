﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Twinkle_milestone2codingtest_09sept3pm.Entities
{
    public partial class ApplicationUser
    {
        public ApplicationUser()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }

       
        public int UserId { get; set; }
   
        public string Name { get; set; }
        
        public string Email { get; set; }
        
        public long? MobileNumber { get; set; }
       
        public int? PinCode { get; set; }
        public string HouseNoBuildingName { get; set; }
      
        public string RoadArea { get; set; }
       
        public string City { get; set; }
       
        public string State { get; set; }

        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
