﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Twinkle_milestone2codingtest_09sept3pm.Entities
{
    public partial class ProductOrder
    {
        
        public int OrderId { get; set; }
       
        public int ProductId { get; set; }
 
        public int UserId { get; set; }
      

        public virtual Product Product { get; set; }
       
        public virtual ApplicationUser User { get; set; }
    }
}
