﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Twinkle_milestone2codingtest_09sept3pm.Entities
{
    public partial class Categoies
    {
        public Categoies()
        {
            Product = new HashSet<Product>();
        }
        [Required]
        public int Catid { get; set; }

        [Required]
        public string CategoryName { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
