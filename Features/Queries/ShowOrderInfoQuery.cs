﻿using MediatR;
using System.Collections.Generic;
using Twinkle_milestone2codingtest_09sept3pm.Entities;

namespace Twinkle_milestone2codingtest_09sept3pm.Features.Queries
{
    public class ShowOrderInfoQuery:IRequest<IEnumerable<ProductOrder>>
    {
        public int UserId { get; set; }
    }
}
