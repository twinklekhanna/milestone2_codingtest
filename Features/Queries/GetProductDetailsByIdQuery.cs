﻿using MediatR;
using Twinkle_milestone2codingtest_09sept3pm.Entities;

namespace Twinkle_milestone2codingtest_09sept3pm.Features.Queries
{
    public class GetProductDetailsByIdQuery:IRequest<Product>
    {
        public int ProductId { get; set; }
    }
}
