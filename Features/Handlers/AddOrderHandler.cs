﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Twinkle_milestone2codingtest_09sept3pm.Entities;
using Twinkle_milestone2codingtest_09sept3pm.Features.Command;
using Twinkle_milestone2codingtest_09sept3pm.Persistance;

namespace Twinkle_milestone2codingtest_09sept3pm.Features.Handlers
{
    public class AddOrderHandler : IRequestHandler<AddOrderCommand, ProductOrder>
    {
        private readonly IGroceryServices _data;

        public AddOrderHandler(IGroceryServices data)
        {
            _data = data;
        }

        public Task<ProductOrder> Handle(AddOrderCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.AddOrder(request.ProductId, request.UserId));
        }
    }
}
