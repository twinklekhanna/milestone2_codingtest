﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Twinkle_milestone2codingtest_09sept3pm.Entities;
using Twinkle_milestone2codingtest_09sept3pm.Features.Queries;
using Twinkle_milestone2codingtest_09sept3pm.Persistance;

namespace Twinkle_milestone2codingtest_09sept3pm.Features.Handlers
{
    public class ShowOrderInfoHandler : IRequestHandler<ShowOrderInfoQuery, IEnumerable<ProductOrder>>
    {
        private readonly IGroceryServices _data;

        public ShowOrderInfoHandler(IGroceryServices data)
        {
            _data = data;
        }

        public async Task<IEnumerable<ProductOrder>> Handle(ShowOrderInfoQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.ShowOrderInfoQuery(request.UserId));

        }
    }
}
