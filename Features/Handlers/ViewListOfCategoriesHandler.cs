﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Twinkle_milestone2codingtest_09sept3pm.Entities;
using Twinkle_milestone2codingtest_09sept3pm.Features.Queries;
using Twinkle_milestone2codingtest_09sept3pm.Persistance;

namespace Twinkle_milestone2codingtest_09sept3pm.Features.Handlers
{
    public class ViewListOfCategoriesHandler : IRequestHandler<ViewListOfCategoriesQuery, IEnumerable<MenuBar>>
    {
        private readonly IGroceryServices _data;

        public ViewListOfCategoriesHandler(IGroceryServices data)
        {
            _data = data;
        }

        public async Task<IEnumerable<MenuBar>> Handle(ViewListOfCategoriesQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.ViewListOfCategories());
        }
    }
}
