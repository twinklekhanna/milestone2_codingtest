﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Twinkle_milestone2codingtest_09sept3pm.Entities;
using Twinkle_milestone2codingtest_09sept3pm.Features.Queries;
using Twinkle_milestone2codingtest_09sept3pm.Persistance;

namespace Twinkle_milestone2codingtest_09sept3pm.Features.Handlers
{
    public class GetProductDetailsByIdHandler : IRequestHandler<GetProductDetailsByIdQuery, Product>
    {
        private readonly IGroceryServices _data;

        public GetProductDetailsByIdHandler(IGroceryServices data)
        {
            _data = data;
        }

        public async Task<Product> Handle(GetProductDetailsByIdQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetProductDetailsById(request.ProductId));

        }
    }
}
