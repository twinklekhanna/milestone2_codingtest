﻿using MediatR;
using Twinkle_milestone2codingtest_09sept3pm.Entities;

namespace Twinkle_milestone2codingtest_09sept3pm.Features.Command
{
    public class AddOrderCommand : IRequest<ProductOrder>
    {

        public int ProductId { get; set; }
        public int UserId { get; set; }
    }
}
