﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Twinkle_milestone2codingtest_09sept3pm.Entities;
using Twinkle_milestone2codingtest_09sept3pm.Features.Command;
using Twinkle_milestone2codingtest_09sept3pm.Features.Queries;

namespace Twinkle_milestone2codingtest_09sept3pm.Controllers
{

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GroceryController : ControllerBase
    {
        private readonly ISender _sender;

        public GroceryController(ISender sender)
        {
            _sender = sender;
        }
        [HttpGet]
        public async Task<IEnumerable<Product>> ViewAllProducts()
        {
            return await _sender.Send(new ViewAllProductQuery());
        }
        [HttpGet]

        public async Task<IEnumerable<MenuBar>> ViewListOfCategories()
        {
            return await _sender.Send(new ViewListOfCategoriesQuery());
        }
        [HttpGet]
        public async Task<Product> GetProductDetailsById(int productid)
        {
            return await _sender.Send(new GetProductDetailsByIdQuery() { ProductId = productid });
        }

        [HttpGet]
        public async Task<IEnumerable<ProductOrder>> ShowOrderInfo(int userid)
        {
            return await _sender.Send(new ShowOrderInfoQuery() { UserId = userid });
        }
        [HttpPost]
        public async Task<ProductOrder> Post(int userid, int productid)
        {
            return await _sender.Send(new AddOrderCommand() { ProductId = productid, UserId = userid });
        }

    }
}
